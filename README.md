# HP ProBook 6550b bios update
updating bios on HP ProBook 6550b with Linux/Windows8+
## prerequisites
- latest Windows bios update standalone package [search here](https://support.hp.com/us-en/drivers/selfservice/hp-probook-6550b-notebook-pc/4173845) → [F.60 Rev.A](https://ftp.hp.com/pub/softpaq/sp73501-74000/sp73917.exe) as of writing, unlikely to ever change. sha1: `1be352f2fe76a1392ce2a60b96df05ec99ef6787` md5: `392dab9a215c8bf8951d16f253febbf9`
- [FreeDos](https://freedos.org/download/), preferrably USB (tested with 1.2)
- find out whether your machine is the `68CDE` or the `68CDF` version by e.g. pressing `[F1]` at start. Replace the asterix (*) with your version's last letter for the following instructions.
## preparing
- unpack the FreeDos `.zip`
- flash the included `.img` file to the external storage device
- extract `sp73917.exe`
- go to `sp73917/hpqflash/`
- extract `sp73917_*.exe`
- go to `sp73917_*/Rompaq/`
- copy `eRompaq.exe` and `68CD*.BIN` to the root folder of your FreeDos media
## flashing
- turn of the laptop and boot to the USB stick `[F9]` (check if booting from USB is enabled if it doesn't work)
- press `[F5]` to skip the installer when prompted (can just mash repeatedly)
- type `erompaq` and press return (enter)
- confirm the installation